# BOC-1 - Project Summary of UBS - Universal Backup Service for SSI Agents

For more information, please contact:

*  Irene Adamski (irene@jolocom.com)
*  Kai Wagner (kai@jolocom.com)
*  Eugeniu Rusu (eugeniu@jolocom.com)


## Introduction

### About Jolocom
Jolocom was founded in 2014 with the express purpose of supporting and accelerating the adoption and development of self-sovereign digital identities.
Since then, it has established itself as a reliable partner in both private and public sectors, through successful R&D pilot projects and the first tailor-made SSI solutions for corporations and organisations.
Within the wider SSI community, Jolocom is involved with a number of associations, including W3C, DIF, INATBA, VSDI, and Bundesblock, with the aim of advancing interoperability and standardisation. Our articles of association reflect this for-purpose mindset and lend credibility to our support of interoperability by precluding mergers and acquisitions.

### The Universal Backup Service (UBS)

The UBS component is intended to enable integrating SSI agents with encrypted backup functionality. More specifically, by integrating the UBS client library, SSI Wallets and Agents will be able to easily create, update, and retrieve encrypted data backups from a remote (either self hosted, or maintained by a backup provider) backup service. Access to encrypted backup documents (as well as the endpoints for creating, updating and deleting them) will be protected using a (configurable) authorization strategy. The UBS client library / service will integrate with existing DID infrastructure to resolve cryptographic material used for encrypting content and authorizing requests.

The UBS component is intended to be an easily deployable, higly configurable, plug and play component, that can suit a wide range of use cases and deployments.

## Summary
## Business Problem

A core challenge of all SSI based identity solutions is the potential loss or destruction of the edge device that holds the identity data. Without a solution to this problem, the lack of convenience for the user puts SSI solutions at a disadvantage compared to centralized identity solution providers, despite the otherwise empowering benefits of decentralized SSI Wallets.

With the Universal Backup Service (UBS) for SSI Agents we plan to provide, the user will have full backup(s) of his or her SSI Agent/Wallet at all times. Giving SSI providers the ability to easily set up self-hosted UBS instances, or use a fully automated, subscription based backup service can make SSI solutions competitive on the Digital Identity Market and bring their self-sovereign capabilities back to the foreground for users.

## Technical Solution

Two distinct software components make up the UBS solution:
- An easily deployable backup server will be developed, based on the emerging [Confidential Storage specification](https://identity.foundation/confidential-storage) developed within the Decentralized Identity Foundation. This component is responsible for persisting / retrieving  encrypted backups and associated encrypted metadata, as well as enforcing authorization policies related to reading / updating the encrypted backups. Developers will be able to interact with an instance of this component using the UBS client library. A more detailed functional specification for the server side component can be found [here](https://gitlab.grnet.gr/essif-lab/business/ubs/deliverables/-/blob/master/UBS_server_functional_specification.md)
- A configurable / easily integratable client side library allowing SSI enabled applications to easily interact with a deployed UBS instance. The library will be configurable to use existing key material listed in the user's DID document for encryption and authorization purposes. Further configuration options will be exposed to the developer integrating the component (e.g. the authorization strategy will be configurable to match different use cases and deployments). A more detailed functional specification for the client side component can be found [here](https://gitlab.grnet.gr/essif-lab/business/ubs/deliverables/-/blob/master/UBS_client_functional_specification.md)

## Integration with the eSSIF-Lab Functional Architecture and Community

Developers will be able to interact with the UBS component using the provided UBS client library. The provisional API of the aforementioned library is described in detail [in the client interface specification document](https://gitlab.grnet.gr/essif-lab/business/ubs/deliverables/-/blob/master/UBS_client_interface_specification.md).
Developers will also be free to interact with the deployed UBS instance directly, via the exposed HTTP REST API described in the [UBS Server interface specification document](https://gitlab.grnet.gr/essif-lab/business/ubs/deliverables/-/blob/master/UBS_server_interface_specification.md).

As mentioned previously, the UBS service is based on a [Confidential Storage ](https://identity.foundation/confidential-storage) compliant storage backend, therefore existing / emerging client libraries for interacting with services conformant to this specification should be usable after some minor modifications to match the slightly more opinionated UBS HTTP API.

Conceptually both the UBS client library, and the UBS server belong on the ["SSI Protocols and Crypto"](https://essif-lab.pages.grnet.gr/framework/docs/functional-architecture#23--ssi-protocols-and-crypto-layer) layer, and can be integrated by all WHIV components.
